﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.IO;

namespace Text_Wrap
{
    class Program
    {
        static void Main(string[] args)
        {
            string inputFileStr = args[0];
            int wordLimit = int.Parse(args[1]);
            var input = File.ReadAllText("text.txt");
            WrapStrings(input, wordLimit);
        }

        static void WrapStrings(string inputStr, int wrapWidth)
        {
            string[] splitStrings = inputStr.Split(" ");
            string printStr = "";
            for (int i = 0; i < splitStrings.Length; i++)
            {
                if (i % wrapWidth == 0 && i != 0)
                {
                    Console.WriteLine(printStr);
                    printStr = splitStrings[i];
                } 
                else if (i == 0)
                {
                    printStr = splitStrings[i];
                }
                else
                {
                    printStr = printStr + " " + splitStrings[i];
                }
            }
            Console.WriteLine(printStr);
        }
    }
}
