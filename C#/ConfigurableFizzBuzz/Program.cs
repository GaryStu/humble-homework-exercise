﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.IO;

namespace ConfigurableFizzBuzz
{
    class FizzBuzz
    {
        private string fizz = "Fizz";
        private string buzz = "Buzz";
        private Tuple<int, int> range;
        
        public FizzBuzz(string fizz, string buzz, Tuple<int,int> range)
        {
            this.fizz = fizz;
            this.buzz = buzz;
            this.range = range;
        }

        public void enumerate()
        {
            string output = "";
            for (int i = range.Item1; i <= range.Item2; i++)
            {
                string temp = "";
                if (i % 3 == 0)
                {
                    temp += this.fizz;
                }
                if (i % 5 == 0)
                {
                    temp += this.buzz;
                }
                if (temp == "")
                {
                    temp = i.ToString();
                }

                output += " " + temp;
            }
            Console.WriteLine(output);

        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            string inputFileStr = args[0];
            string fizz = args[1];
            string buzz = args[2];
            var input = File.ReadAllText(inputFileStr);
            string[] ranges = input.Split("\n");
            for (int i = 0; i < ranges.Length; i++)
            {
                string[] rangeStr = ranges[i].Split(",");
                Tuple<int, int> range = new Tuple<int, int>(int.Parse(rangeStr[0]), int.Parse(rangeStr[1]));
                FizzBuzz fizzBuzz = new FizzBuzz(fizz, buzz, range);
                fizzBuzz.enumerate();
            }
        }
    }
}
