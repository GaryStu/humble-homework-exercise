import * as fs from "fs";

function loadFromJSON(): object {
    return JSON.parse(fs.readFileSync("db.json", "utf8"));
}

function saveToJSON(db: object): void {
    fs.writeFileSync("db.json", JSON.stringify(db));
}

function save(db: object, key: string, value:string) : void {
    db[key] = value;
}

function load(db: object, key:string) : string {
    if (key in db) {
        return db[key];
    } else {
        return "NULL";
    }
}

function del(db: object, key:string): void {
    if (key in db) {
        delete db[key];
    }
}



const args = process.argv.splice(2);
let command = args[0];
let db = loadFromJSON();
if (command == "save") {
    save(db, args[1], args[2]);
} else if (command == "load") {
    load(db, args[1]);
} else if (command == "delete") {
    del(db, args[1]);
}
saveToJSON(db);

/*
interface defaultDB {
    set(key: string, value: string): void;
    get(key: string): string | null;
    delete(key: string): void;
}
class NaiveDatabase implements defaultDB {
    db: object;

    constructor(db: object) {
        this.db = db;
    }

    get(key: string): any | null {
        return this.db[key.toString()];
    }

    set(key: string, value: string): void {
        this.db[key.toString()] = value;
    }

    delete(key: string): void {
        delete this.db[key];
    }

}
 */


