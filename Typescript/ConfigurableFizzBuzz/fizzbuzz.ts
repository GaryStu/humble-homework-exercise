import {Fizzbuzz} from "./fbclass";
import * as fs from "fs";

//main code
const args = process.argv.splice(2);
let inputFileStr : string = args[0];
const rangesInput = fs.readFileSync(inputFileStr , 'utf8');
let ranges = rangesInput.split("\n");
for (let i = 0; i < ranges.length; i++) {
    let fromTo = ranges[i].split(',');
    let fizzBuzz = new Fizzbuzz(+fromTo[0] , +fromTo[1], args[1], args[2]);
    fizzBuzz.enumerate();
}