class Fizzbuzz {
    from: number;
    to: number;
    fizz: string;
    buzz: string;

    constructor(from: number, to: number, fizz: string = "Fizz", buzz:string = "Buzz") {
        this.from = from;
        this.to = to;
        this.fizz = fizz;
        this.buzz = buzz;
    }

    enumerate() {
        let output = "";
        for (let i = this.from; i <= this.to; i++) {
            let temp = "";
            if (i % 3 == 0) { temp += this.fizz; }
            if (i % 5 == 0) { temp += this.buzz; }
            if (temp == "") { temp = String(i);}
            output += " " + temp;
        }
        console.log(output);
    }
}

export {Fizzbuzz}
