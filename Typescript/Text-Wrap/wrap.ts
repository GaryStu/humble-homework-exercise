import * as fs from "fs";

const args = process.argv.splice(2);
let inputFileStr : string = args[0];
const input = fs.readFileSync(inputFileStr , 'utf8');
let wordLimit : number = +args[1];

function WrapStrings(inputStr :string, wrapWidth: number) : void {
    let splitStrings = inputStr.split(' ');
    let counter = 0;
    let printStr = "";
    for (const str of splitStrings) {
        if (counter % wrapWidth == 0 && counter != 0) {
            console.log(printStr);
            printStr = str;
        } else if (counter == 0) {
            printStr = str;
        } else {
            printStr = printStr + " " + str;
        }
        counter++;
    }
    console.log(printStr);
}

WrapStrings(input, wordLimit);







